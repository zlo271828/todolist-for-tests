import {all, call, take, put, takeLatest, spawn } from 'redux-saga/effects';
import firebase from 'firebase';
import {eventChannel} from 'redux-saga';
import {reset} from 'redux-form';
import {appName} from '../config';
import {Record, OrderedMap} from 'immutable';
import {fbDataToEntities} from './utils';
import {createSelector} from 'reselect';

/**
 * Constants
 * */
export const moduleName = 'tasks';
const prefix = `${appName}/${moduleName}`;

export const FETCH_ALL_TASKS_SUCCESS = `${prefix}/FETCH_ALL_TASKS_SUCCESS`;
export const FETCH_ALL_TASKS_REQUEST = `${prefix}/FETCH_ALL_TASKS_REQUEST`;
export const FETCH_ALL_TASKS_ERROR = `${prefix}/FETCH_ALL_TASKS_ERROR`;

export const ADD_TASK_SUCCESS = `${prefix}/ADD_TASK_SUCCESS`;
export const ADD_TASK_REQUEST = `${prefix}/ADD_TASK_REQUEST`;
export const ADD_TASK_ERROR = `${prefix}/ADD_TASK_ERROR`;

export const REMOVE_TASK_SUCCESS = `${prefix}/REMOVE_TASK_SUCCESS`;
export const REMOVE_TASK_REQUEST = `${prefix}/REMOVE_TASK_REQUEST`;
export const REMOVE_TASK_ERROR = `${prefix}/REMOVE_TASK_ERROR`;

export const FINISH_TASK_SUCCESS = `${prefix}/FINISH_TASK_SUCCESS`;
export const FINISH_TASK_REQUEST = `${prefix}/FINISH_TASK_REQUEST`;
export const FINISH_TASK_ERROR = `${prefix}/FINISH_TASK_ERROR`;

export const EDIT_TASK_SUCCESS = `${prefix}/EDIT_TASK_SUCCESS`;
export const EDIT_TASK_REQUEST = `${prefix}/EDIT_TASK_REQUEST`;
export const EDIT_TASK_ERROR = `${prefix}/EDIT_TASK_ERROR`;

export const PRIORITY_CHANGE_REQUEST = `@@redux-form/CHANGE`;
export const PRIORITY_CHANGE_SUCCESS = `${prefix}/PRIORITY_CHANGE_SUCCESS`;

/**
 * Reducer
 * */
export const ReducerRecord = Record({
    entities: new OrderedMap({}),
    loading: false,
    priority: 'high',
});

export const TaskRecord = Record({
    uid: null,
    description: null,
    priority: null,
    dateTo: null,
    dateFrom: null,
    finished: false,
});

export default function reducer(state = new ReducerRecord(), action) {
    const {type, payload, error} = action;

    switch (type) {
        case FETCH_ALL_TASKS_REQUEST:
            return state
                .set('loading', true);

        case FETCH_ALL_TASKS_SUCCESS:
            return state
                .set('loading', false)
                .set('entities', fbDataToEntities(payload, TaskRecord));

        case FETCH_ALL_TASKS_ERROR:
            return error;

        case PRIORITY_CHANGE_SUCCESS:
            return state
                .set('priority', payload);
        default:
            return state;
    }
}

/**
 * Selectors
 * */

export const stateSelector = (state) => state[moduleName];
export const prioritySelector = (state) => state[moduleName].priority;
export const entitiesSelector = createSelector(stateSelector, (state) => state.entities);
export const taskListSelector = createSelector(entitiesSelector, prioritySelector,
    (entities, priority) => entities.valueSeq().toArray().filter((task) => {
        if (priority === 'all') return true;
        return task.priority === priority
    }));

/**
* Action Creators
* */

export function fetchAll() {
    return {
        type: FETCH_ALL_TASKS_REQUEST
    }
}

export function addTask(task) {

    return {
        type: ADD_TASK_REQUEST,
        payload: task,
    }
}

export function removeTask(taskId) {
    return {
        type: REMOVE_TASK_REQUEST,
        payload: taskId,
    }
}

export function finishTask(taskId) {
    return {
        type: FINISH_TASK_REQUEST,
        payload: taskId,
    }
}

export function editTask(task) {
    return {
        type: EDIT_TASK_REQUEST,
        payload: task,
    }
}

/**
* Sagas
* */

const createTaskSocket = () => eventChannel(emmit => {
    const ref = firebase.database().ref('tasks');
    const callback = (data) => emmit({data});
    ref.on('value', callback);

    return () => {
        console.log('Unsubscribing from "firebase/tasks"');
        ref.off('value', callback)
    }
});

export const realtimeTasksSync = function* () {
    const chan = yield call(createTaskSocket);
    try {
        while (true) {
            const {data} = yield take(chan);

            yield put({
                type: FETCH_ALL_TASKS_SUCCESS,
                payload: data.val(),
            })
        }
    } finally {
        yield call([chan, chan.close])
    }
};

export const fetchAllSaga = function* () {
    const tasksRef = firebase.database().ref('tasks');

    try {
        const data = yield call([tasksRef, tasksRef.once], 'value');
        yield put({
            type: FETCH_ALL_TASKS_SUCCESS,
            payload: data.val(),
        })
    } catch (error) {
        yield put({
            type: FETCH_ALL_TASKS_ERROR,
            error
        })
    }
};

export const addTaskSaga = function* (action) {
    const taskRef = firebase.database().ref('tasks');
    const task = {
        ...action.payload,
        dateFrom: Date.now(),
        finished: false,
    };
    try {
        const ref = yield call([taskRef, taskRef.push], task);
        yield put({
            type: ADD_TASK_SUCCESS,
            payload: {
                ...action.payload,
                uid: ref.key
            }
        });

        yield put(reset('task'))
    } catch (error) {
        yield put({
            type: ADD_TASK_ERROR,
            error,
        })
    }
};

export const removeTaskSaga = function* (action) {
    const taskId = action.payload;
    const taskRef = firebase.database().ref(`tasks/${taskId}`);
    try {
        yield call([taskRef, taskRef.remove]);
        yield put({
            type: REMOVE_TASK_SUCCESS,
        })
    } catch (error) {
        yield put({
            type: REMOVE_TASK_ERROR,
            error,
        })
    }
};

export const finishTaskSaga = function* (action) {
    const taskId = action.payload;
    const taskRef = firebase.database().ref();
    const updates = {};
    updates[`tasks/${taskId}/finished`] = true;
    try {
        yield call([taskRef, taskRef.update], updates);
        yield put({
            type: FINISH_TASK_SUCCESS,
        })
    } catch (error) {
        yield put({
            type: FINISH_TASK_ERROR,
            error,
        })
    }
};

export const editTaskSaga = function* (action) {
    const task = action.payload;
    const taskRef = firebase.database().ref();
    const updates = {};
    updates[`tasks/${task.taskId}`] = {
        description: task.description,
        priority: task.priority,
        dateTo: task.dateTo,
        dateFrom: Date.now(),
        finished: false,
    };
    try {
        yield call([taskRef, taskRef.update], updates);
        yield put({
            type: EDIT_TASK_SUCCESS,
        })
    } catch (error) {
        yield put({
            type: EDIT_TASK_ERROR,
            error,
        })
    }
};

export const changePrioritySaga = function* (action) {
    const {payload, meta: {form}} = action;
    if (form === 'priority') yield put({
        type: PRIORITY_CHANGE_SUCCESS,
        payload,
    })
};

export function* saga() {
    yield spawn(realtimeTasksSync);
    yield all([
      takeLatest(FETCH_ALL_TASKS_REQUEST, fetchAllSaga),
      takeLatest(ADD_TASK_REQUEST, addTaskSaga),
      takeLatest(REMOVE_TASK_REQUEST, removeTaskSaga),
      takeLatest(FINISH_TASK_REQUEST, finishTaskSaga),
      takeLatest(EDIT_TASK_REQUEST, editTaskSaga),
      takeLatest(PRIORITY_CHANGE_REQUEST, changePrioritySaga),
    ]);
}
