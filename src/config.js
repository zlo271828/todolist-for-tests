import firebase from 'firebase';

export const appName = 'allmax-test-todolist';
export const firebaseConfig = {
    apiKey: "AIzaSyCyYNJgXUDXmxwL7_aftT9Ki6YYtlutWE8",
    authDomain: `${appName}.firebaseapp.com`,
    databaseURL: `https://${appName}.firebaseio.com`,
    projectId: `${appName}`,
    storageBucket: `${appName}.appspot.com`,
    messagingSenderId: "1025207712009"
};

firebase.initializeApp(firebaseConfig);
