import React from 'react';
import TaskList from './TaskList/TaskList';
import {Layout} from 'antd';

const {Content} = Layout;

class Root extends React.Component {

    render() {

        return (
            <div>
                <Content style={{padding: '0 50px'}}>
                    <div style={{background: '#fff', padding: 24, minHeight: 280}}>
                        <TaskList/>
                    </div>
                </Content>
            </div>
    );
    };
    }

    export default Root;
