import React from 'react';
import {reduxForm, Field} from 'redux-form';

class TaskForm extends React.Component {
    render() {
        return (
            <div>
                <form>
                    <Field name="priority" component="select">
                        <option value="high">high</option>
                        <option value="mid">mid</option>
                        <option value="low">low</option>
                        <option value="all">all</option>
                    </Field>
                </form>
            </div>
        );
    };
}

export default reduxForm({
    form: 'priority',
})(TaskForm);
