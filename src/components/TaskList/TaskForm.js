import React from 'react';
import {reduxForm, Field} from 'redux-form';
import ErrorField from '../common/ErrorField';

class TaskForm extends React.Component {
    render() {
        const {handleSubmit} = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit}>
                    <Field name="description" component={ErrorField}/>
                    <label>priority</label>
                    <Field name="priority" component="select">
                        <option/>
                        <option value="low">low</option>
                        <option value="mid">mid</option>
                        <option value="high">high</option>
                    </Field>
                    <Field name="dateTo" type="date" component={ErrorField}/>
                    <div>
                        <input type="submit"/>
                    </div>
                </form>
            </div>
        );
    };
}

function validate({description, dateTo, priority}) {
    const error = {};
    const date = new Date(dateTo);
    if (!priority) error.priority = 'Choose priority';
    if (!description) error.description = 'Enter some description';
    if (date && date < Date.now()) error.dateTo = 'Enter valid dateTo';
    return error;
}

export default reduxForm({
    form: 'task',
    validate,
})(TaskForm);
