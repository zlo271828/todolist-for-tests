import {createStore, applyMiddleware, compose} from 'redux';
import reducer from './reducer';
import {routerMiddleware} from 'react-router-redux';
import history from '../history';
import rootSaga from './saga';
import createMiddleware from 'redux-saga';

const sagaMiddleware = createMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware, routerMiddleware(history)));

const store = createStore(reducer, enhancer);

window.store = store;

sagaMiddleware.run(rootSaga);

export default store;
