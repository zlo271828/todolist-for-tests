import {all} from 'redux-saga/effects';
import {saga as tasksSaga} from '../ducks/tasks';

export default function* rootSaga() {
    yield all([
       tasksSaga(),
    ]);
};
